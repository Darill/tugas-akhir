import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { View, Text, TouchableOpacity, FlatList, StyleSheet, TextInput, SafeAreaView, } from 'react-native'
import { Ionicons } from '@expo/vector-icons';

const baseURL = "https://sanbers-news-api.herokuapp.com/api"

const client = axios.create({
    baseURL: baseURL,
})

const Tugas14 = () => {
    const [title, setTitle] = useState("")
    const [value, setValue] = useState("")
    const [data, setData] = useState([])
    const [buttonMode, setButtonMode] = useState("Simpan")
    const [selectedNews, setSelectedNews] = useState({})

    const handleError = (err) => {
        console.error("Error Status: ", err.message);
        console.error("Error Message: ", err.response.data);
    }
    const Getdata = () => {
        client.get(`/news`)
            .then((res) => {
                const datas = res.data.data
                console.log("res: ", datas);
                setData(datas)
            })
            .catch((err) => {
                handleError(err)
            })
    }
    const addNews = (title, value) => {
        client.post(`/news`, { title, value })
            .then((res) => {
                console.log("res: ", res);
                setTitle("")
                setValue("")
            })
            .catch((err) => {
                handleError(err)
            })
    }
    const editNews = (id, title, value) => {
        client.put(`/news/${id}`, { title, value })
            .then((res) => {
                console.log("res: ", res);
                setTitle("")
                setValue("")
                Getdata()
                setButtonMode("Simpan")
            })
            .catch((err) => {
                handleError(err)
            })
    }
    const deleteNews = (id) => {
        client.delete(`/news/${id}`)
            .then((res) => {
                console.log("res: ", res);
                Getdata()
            })
            .catch((err) => {
                handleError(err)
            })
    }

    const submit = async () => {
        try {
            if (buttonMode == "Simpan") {
                await addNews(title, value);
            } else {
                console.log(selectedNews);
                await editNews(selectedNews._id, title, value)
            }
            Getdata()
        } catch (error) {
            handleError(err)
        }
    };

    const onSelectItem = (item) => {
        console.log(item);
        setSelectedNews(item)
        setTitle(item.title)
        setValue(item.value)
        setButtonMode("Update")
    }
    const onDelete = (item) => deleteNews(item._id)

    useEffect(() => {
        Getdata()
    }, [])

    return (
        <View>
            <FlatList
                style={styles.FlatList}
                data={data}
                keyExtractor={(item, index) => `${item._id}-${index}`}
                renderItem={({ item }) => (
                    <View style={styles.container}>
                        <TouchableOpacity
                            onPress={() => onSelectItem(item)}
                            style={{ width: '75%' }}
                        >
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.value}>{item.value}</Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => onDelete(item)}
                            >
                                <Ionicons name="trash-outline" size={24} color="black" />
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            />
            <SafeAreaView>
                <View style={styles.inputBerita}>
                    <Text style={{ fontSize: 18, marginVertical: 10 }}>Judul</Text>
                    <TextInput
                        style={styles.txtInputJudul}
                        placeholder="Judul Berita"
                        placeholderTextColor="grey"
                        value={title}
                        onChangeText={(value) => setTitle(value)}
                    />
                    <Text style={{ fontSize: 18, marginVertical: 10 }}>Content</Text>
                    <TextInput
                        placeholder="Masukan Berita"
                        style={styles.txtInputBerita}
                        placeholderTextColor="grey"
                        value={value}
                        onChangeText={(value) => setValue(value)}
                    />
                    <TouchableOpacity style={styles.button} onPress={submit}>
                        <Text style={styles.buttonTxt}>Send</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    FlatList: {
        maxHeight: 300
    },
    container: {
        flex: 2,
        paddingHorizontal: 20,
        paddingTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 30
    },
    value: {
        fontSize: 20
    },
    txtInputJudul: {
        borderRadius: 10,
        borderWidth: 1,
        height: 40,
        padding: 10,
        fontSize: 18
    },
    txtInputBerita: {
        borderWidth: 1,
        height: 60,
        padding: 10,
        fontSize: 18,
        borderRadius: 10
    },
    inputBerita: {
        paddingHorizontal: 20,
        marginVertical: 40,
        elevation: 5
    },
    button: {
        alignSelf: 'center',
        backgroundColor: 'blue',
        paddingHorizontal: 15,
        paddingVertical: 10,
        width: '50%',
        marginVertical: 20,
        borderRadius: 10
    },
    buttonTxt: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center'
    }
})

export default Tugas14
