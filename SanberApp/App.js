import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Headers from './Final Project/component/headers';
import Home from './Final Project/component/home';
import MovieRender from './Final Project/page';


export default function App() {
  return (
    <View style={styles.container}>
      <MovieRender />
      <StatusBar
        style='light'
        translucent={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
