import React, { useContext } from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import 'react-native-gesture-handler';
import LoginScreen from './loginScreen';
import AboutScreen from './aboutScreen';
import { LoginContext } from '../Tugas 16/context';
import { Text } from 'react-native';

const Stack = createNativeStackNavigator()

const IndexNav = () => {
    const [loginStatus, setLoginStatus] = useContext(LoginContext)
    console.log(loginStatus)
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login" component={LoginScreen}>
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="About" component={AboutScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default IndexNav;