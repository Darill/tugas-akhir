import React, { Component, useContext, useState } from 'react'
import { StyleSheet, Text, View, Image, TextInput, Button, ScrollView, TouchableOpacity } from 'react-native'
import Logo from './img/Logo.png';
const colors = {
    lightBlue: "#3EC6FF",
    darkBlue: "#003366",
    grayBackground: "#EFEFEF"
}
import { Ionicons } from '@expo/vector-icons';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LoginContext } from '../Tugas 16/context';

export default function LoginScreen({navigation}) {

    const Stack = createNativeStackNavigator()
    const [visible, setVisible] = useState(true)
    const handleClick = () => {
        setVisible(!visible)
    }
    const [inputEmail, setInputemail] = useState("")
    const [inputPassword, setInputPassword] = useState("")

    const [auth, setAuth, loginStatus, setLoginStatus, IsError, setIsError] = useContext(LoginContext);
    const submit = () => {
        setAuth([...auth, {id: Math.floor(Math.random() * 10), email: inputEmail, password: inputPassword}])
        setInputemail("")
        setInputPassword("")
        console.log(auth)
    }

    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={{ alignItems: 'center', marginTop: 30 }}>
                    <Image source={Logo} />
                </View>
                <View style={styles.inputWrapper}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Name"
                        placeholderTextColor="grey"
                        value={inputEmail}
                        onChangeText={(value) => { setInputemail(value) }}
                    />
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            placeholder="Password"
                            placeholderTextColor="grey"
                            style={styles.inputText1}
                            secureTextEntry={visible ? true : false}
                            value={inputPassword}
                            onChangeText={(value) => { setInputPassword(value) }}
                        />
                        {
                            visible ?
                                <Ionicons name="eye-outline" size={24} color="black" onPress={handleClick} />
                                :
                                <Ionicons name="eye-off-outline" size={24} color="black" onPress={handleClick} />
                        }

                    </View>
                </View>
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity style={styles.button} onPress={submit}>
                        <Text style={styles.txtLogin}>Masuk</Text>
                    </TouchableOpacity>
                    <Text>atau</Text>
                    <TouchableOpacity style={styles.button1}>
                        <Text style={styles.txtDaftar}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 20
    },
    inputText: {
        borderBottomWidth: 1,
        // borderWidth: 1,
        marginVertical: 10,
        padding: 5,
        width: '75%',
        color: 'black',
        fontSize: 20
    },
    inputText1: {
        borderBottomWidth: 1,
        // borderWidth: 1,
        marginVertical: 10,
        padding: 5,
        width: '70%',
        color: 'black',
        fontSize: 20
    },
    inputWrapper: {
        marginVertical: 50,
        alignItems: 'center',
    },
    button: {
        borderWidth: 2,
        borderColor: '#C4C4C4',
        width: '50%',
        marginVertical: 5,
        height: 40,
        justifyContent: 'center',
        borderRadius: 10
    },
    button1: {
        backgroundColor: '#C4C4C4',
        width: '50%',
        marginVertical: 5,
        height: 40,
        justifyContent: 'center',
        borderRadius: 10
    },
    buttonWrapper: {
        alignItems: 'center',
    },
    txtLogin: {
        textAlign: 'center',
        fontSize: 18,
        color: '#C4C4C4',
        fontWeight: 'bold'
    },
    txtDaftar: {
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold'
    }

})
