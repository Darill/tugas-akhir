import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button, FlatList, TouchableOpacity, SafeAreaView } from 'react-native'
import { Data } from './data'
export default function Home({ route, navigation }) {
    const { username } = route.params;
    // console.log(username)
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat = (num) => {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    const updateHarga = (price) => {
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
    }
    //? #Bonus (10 poin) -- HomeScreen.js --
    //? agar harga dapat update misal di tambah lebih dari 1 item atau lebih -->

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text>Selamat Datang, </Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}> {currencyFormat(totalPrice)} </Text>
                </View>
            </View>
            <View style={{ alignItems: 'center', marginBottom: 100}}>
                {/* //? #Soal No 2 (15 poin) -- Home.js -- Function Home
            //? Buatlah 1 komponen FlatList dengan input berasal dari data.js   
            //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal) -->
            
            //? #Soal No 3 (15 poin) -- HomeScreen.js --
             //? Buatlah styling komponen Flatlist, agar dapat tampil dengan baik di device untuk layouting bebas  --> */

                }

                    <FlatList
                        numColumns={2}
                        data={Data}
                        keyExtractor={(item, index) => `${item.id}-${index}`}
                        renderItem={({ item }) => {
                            return (
                                <View style={{ borderWidth: 1, padding: 10, margin: 5, borderRadius: 10 }}>
                                    <Image
                                        source={item.image}
                                        style={{ width: 150, height: 150 }}
                                    />
                                    <Text style={{fontWeight:'bold', fontSize: 18}}>{item.title}</Text>
                                    <Text>Rp. {item.harga}</Text>
                                    <TouchableOpacity style={{backgroundColor: 'blue', width: '50%', alignItems: 'center', borderRadius: 5, marginTop: 10, padding: 5}}
                                        onPress={() => updateHarga(item.harga)}
                                    >
                                        <Text style={{color: 'white'}}>Beli</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                        }}
                    />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    content: {
        width: 150,
        height: 220,
        margin: 5,
        borderWidth: 1,
        // alignItems: 'center',
        borderRadius: 5,
        borderColor: 'grey',
    },

})
