import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

export const MovieContext = createContext();

export const MovieProvider = ({ children }) => {
    const [data, setData] = useState([]);
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loadMore, setLoadMore] = useState(`https://fakestoreapi.com/products`)
    const [loading, setLoading] = useState(true)
    const handleError = (err) => {
        console.error("Error Status: ", err.message);
        console.error("Error Message: ", err.response.data);
    }
    useEffect(() => {
        setLoading(true)
        axios.get(loadMore)
            .then((res) => {
                setData(res.data)
                setLoading(false)
            })
            .catch((err) => {
                handleError(err)
            })
    }, [])



    return (
        <MovieContext.Provider value={[data, setData, loadMore, setLoadMore, loading, setLoading, email, setEmail, password, setPassword]}>
            {children}
        </MovieContext.Provider>
    );

};
