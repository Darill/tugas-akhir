import React from "react";
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from "react-native";
import { Ionicons, AntDesign } from '@expo/vector-icons';
const Headers = ({ navigation }) => {
    return (
        <View style={styles.container2}>
            <TextInput
                placeholder="Search"
                placeholderTextColor="grey"
                style={styles.textinput}
            />
            <TouchableOpacity>
                <AntDesign name="shoppingcart" size={22} color="black" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
                <Ionicons name="person-circle-outline" size={24} color="black" />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container2: {
        flex: 0.1,
        // borderBottomWidth: 0.2,
        elevation: 5,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
        justifyContent: 'space-around',
        backgroundColor: 'white'
    },
    titleHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    textinput: {
        height: 40,
        width: '80%',
        borderWidth: 0.5,
        borderRadius: 10,
        paddingHorizontal: 5,
        borderColor: 'black',
        color: 'black'
    }
})
export default Headers;