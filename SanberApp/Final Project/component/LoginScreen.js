import React, { useState } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import { initializeApp, getApps } from 'firebase/app'
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth'
import { useContext } from 'react/cjs/react.development';
import { MovieContext } from '../context/fetch';
import { Entypo } from '@expo/vector-icons';

const LoginScreen = ({ navigation }) => {

    const [data, setData, loadMore, setLoadMore, loading, setLoading, email, setEmail, password, setPassword] = useContext(MovieContext)

    const firebaseConfig = {
        apiKey: "AIzaSyDoeACuVGMxWcG2KHW2dL2KpShDAai5F-4",
        authDomain: "finalprojectrn-a0d1f.firebaseapp.com",
        databaseURL: "https://finalprojectrn-a0d1f-default-rtdb.firebaseio.com",
        projectId: "finalprojectrn-a0d1f",
        storageBucket: "finalprojectrn-a0d1f.appspot.com",
        messagingSenderId: "469169593706",
        appId: "1:469169593706:web:6085779c3a439f1637d913"
    };

    if (!getApps().length) {
        initializeApp(firebaseConfig);
    }

    const submit = () => {
        const data = {
            email, password
        }
        console.log(data);
        const auth = getAuth()
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                navigation.navigate("Home")
                const user = userCredential.user
                console.log("Login Berhasil")
            })
            .catch(() => {
                console.log("Login Gagal")
            })
    }

    return (
        <View style={styles.container}>
            <View style={{ alignSelf: 'center', alignItems: 'center' }}>
                <Entypo name="shop" size={120} color="white" />
                <Text style={{ color: 'white', fontSize: 20 }}>CommerceLite</Text>
            </View>
            <View style={styles.inputContain}>
                <TextInput
                    placeholder="Email"
                    placeholderTextColor='white'
                    value={email}
                    onChangeText={(value) => setEmail(value)}
                    style={styles.inputTxt}
                />
                <TextInput
                    placeholder="Password"
                    placeholderTextColor='white'
                    value={password}
                    onChangeText={(value) => setPassword(value)}
                    style={styles.inputTxt}
                    secureTextEntry={true}
                />
            </View>
            <View style={styles.reg}>
                <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                    <Text style={styles.create}>Create Account? Register</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={submit} style={styles.button}>
                <Text style={styles.txt}>Login</Text>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#47D844'
    },
    inputContain: {
        width: '90%',
        alignSelf: 'center'
    },
    inputTxt: {
        borderWidth: 1.5,
        marginTop: 15,
        borderRadius: 10,
        height: 40,
        padding: 10,
        borderColor: 'white',
        color: 'white'
    },
    reg: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 20,
        marginTop: 5
    },
    create: {
        color: 'white',
        paddingHorizontal: 20
    },
    button: {
        // backgroundColor: '#47D844',
        // elevation: 5,
        borderWidth: 1,
        borderColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 10,
        width: '90%',
        borderRadius: 10,
        alignSelf: 'center'
    },
    txt: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    }
})

export default LoginScreen;
