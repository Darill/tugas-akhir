import React, { useContext } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Ionicons, AntDesign, EvilIcons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { getAuth, signOut } from '@firebase/auth';
import { MovieContext } from '../context/fetch';

const ProfileScreen = ({ route, navigation }) => {
    const [data, setData, loadMore, setLoadMore, loading, setLoading, email, setEmail, password, setPassword] = useContext(MovieContext)
    const LogOut = () => {
        const auth = getAuth()
        signOut(auth)
            .then(() => {
                navigation.navigate("Login")
                console.log("Berhasil LogOut");
            })
    }
    return (
        <View style={styles.container}>

            <View style={styles.emailContent}>
                <Ionicons name="person-circle-outline" size={80} color="white" />
                <View style={styles.profileSet}>
                    <View>
                        <Text style={styles.email}>{email}</Text>
                        <TouchableOpacity style={styles.setting}>
                            <Text style={styles.edit}>edit Profile</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={LogOut}>
                        <Ionicons name="exit-outline" size={34} color="white" />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.dll}>
                <View style={styles.icon}>
                    <TouchableOpacity style={styles.icons}>
                        <AntDesign name="hearto" size={26} color="black" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.icons}>
                        <EvilIcons name="bell" size={30} color="black" />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.icons} onPress={() => navigation.navigate("Home")}>
                        <AntDesign name="shoppingcart" size={26} color="black" />
                    </TouchableOpacity>
                </View>
                <View style={styles.dllIcon}>
                    <TouchableOpacity style={styles.dllBox}>
                        <MaterialCommunityIcons name="google-maps" size={24} color="black" />
                        <Text style={styles.txt}>Delivery Status</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.dllIcon}>
                    <TouchableOpacity style={styles.dllBox}>
                        <MaterialIcons name="payment" size={24} color="black" />
                        <Text style={styles.txt}>Payment Method</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.dllIcon}>
                    <TouchableOpacity style={styles.dllBox}>
                        <MaterialIcons name="privacy-tip" size={24} color="black" />
                        <Text style={styles.txt}>Privacy Policy</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    emailContent: {
        flex: 0.2,
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#47D844',
        // elevation: 5,
        borderBottomRightRadius: 30,
        borderBottomLeftRadius: 30,
    },
    email: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white'
    },
    edit: {
        color: 'white'
    },
    profileSet: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '80%',
        alignItems: 'center',
        paddingRight: 20,
        paddingLeft: 10
    },
    setting: {
        borderWidth: 1,
        width: '65%',
        paddingHorizontal: 10,
        borderRadius: 5,
        borderColor: 'white',
        paddingVertical: 3,
        marginTop: 5
    },
    icon: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'space-around',
        width: '80%',
        paddingVertical: 10,
    },
    icons: {
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 15,
        elevation: 5,
    },
    dll: {
        backgroundColor: 'white',
        flex: 1,
    },
    dllIcon: {
        backgroundColor: 'white',
        elevation: 5,
        marginTop: 20,
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingVertical: 20,
        alignItems: 'center',
        borderRadius: 20,
        width: '90%',
        alignSelf: 'center'
    },
    txt: {
        paddingLeft: 10,
        fontSize: 16
    },
    dllBox: {
        flexDirection: 'row'
    }
})

export default ProfileScreen;
