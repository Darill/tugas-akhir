import React, { useContext, useState } from "react";
import { FlatList, Text, TextInput, TouchableOpacity, Image, ActivityIndicator, StyleSheet, View } from "react-native";
import { MovieContext } from "../context/fetch";
import { AntDesign, Ionicons } from '@expo/vector-icons';
// import Headers from "./headers";

const Home = ({ navigation }) => {
    const [data, setData, loadMore, setLoadMore, loading, setLoadin] = useContext(MovieContext)
    // console.log(data)
    return (
        <>
            <View style={styles.container}>
                {/* Headers */}
                <View style={styles.container2}>
                    <TextInput
                        placeholder="Search"
                        placeholderTextColor="white"
                        style={styles.textinput}
                    />
                    <TouchableOpacity>
                        <AntDesign name="shoppingcart" size={28} color="white" />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => navigation.navigate("Profile")}>
                        <Ionicons name="person-circle-outline" size={28} color="white" />
                    </TouchableOpacity>
                </View>
                {/* Load */}

                {
                    loading ? <View style={styles.loading}>
                        <ActivityIndicator size="small" color="#0000ff" />
                        <Text>Tunggu Sebentar...</Text>
                    </View> : null
                }

                {/* Data */}
                <FlatList
                    style={styles.flatlist}
                    data={data}
                    numColumns={2}
                    keyExtractor={(item, index) => `${item.id}-${index}`}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={styles.containerList} onPress={() => navigation.navigate("Detail", { title: item.title, image: item.image, price: item.price, desc: item.description, rate: item.rating.rate, count: item.rating.count, category: item.category })}>
                            <Image source={{ uri: item.image }} style={styles.imageList} />
                            <Text style={styles.title}>{item.title}</Text>
                            <Text style={styles.price}>${item.price}</Text>
                            <View style={styles.rate}>
                                <AntDesign name="star" size={18} style={{ paddingRight: 2 }} color="yellow" />
                                <Text style={styles.txtRate}>{item.rating.rate} | </Text>
                                <Text style={styles.txtRate}>Terjual {item.rating.count}</Text>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        </>
    )
}

const styles = StyleSheet.create({

    // Headers
    container2: {
        flex: 0.1,
        // borderBottomWidth: 0.2,
        elevation: 5,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 5,
        justifyContent: 'space-around',
        backgroundColor: '#47D844'
    },
    titleHeader: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white'
    },
    textinput: {
        height: 40,
        width: '80%',
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 10,
        borderColor: 'white',
        color: 'black'
    },
    // FlatList

    container: {
        flex: 1,
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    flatlist: {
        paddingBottom: 100,
        flex: 1
    },
    imageList: {
        width: 150,
        height: 200,
        alignSelf: 'center',
    },
    title: {
        fontSize: 17,
        paddingTop: 5,
        color: 'black'
    },
    price: {
        fontWeight: 'bold',
        fontSize: 17,
        paddingVertical: 5,
        color: 'black',
    },
    rate: {
        flexDirection: 'row',
        color: 'black',
        alignItems: 'center'
    },
    txtRate: {
        color: 'black',
    },
    containerList: {
        flex: 1,
        margin: 5,
        paddingHorizontal: 25,
        paddingVertical: 20,
        justifyContent: 'space-around',
        borderBottomWidth: 0.5,
        backgroundColor: 'white',
        elevation: 5,
        borderRadius: 20
    }
})
export default Home