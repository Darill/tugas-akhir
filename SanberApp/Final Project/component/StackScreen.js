import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import DetailScreen from "./DetailScreen";
import Headers from "./headers";
import Home from "./home";
import LoginScreen from "./LoginScreen";
import ProfileScreen from "./ProfileScreen";
import RegisterScreen from "./RegisterScreen";

const StackScreen = () => {
    const Stack = createNativeStackNavigator()
    return (
        <NavigationContainer>
            <Stack.Navigator name="Login" component={LoginScreen} >
                <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="Detail" component={DetailScreen} options={{
                    headerStyle: {
                        backgroundColor: '#47D844',
                    }, headerTintColor: '#fff',
                }} />
                <Stack.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Header" component={Headers} options={{ headerShown: false }} />
            </Stack.Navigator >
        </NavigationContainer>
    )
}

export default StackScreen