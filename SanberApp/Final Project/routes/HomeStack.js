import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import Home from '../component/home';
import LoginScreen from '../component/LoginScreen';
import DetailScreen from '../component/DetailScreen';
const screen = {
    Login: {
        screen: LoginScreen
    },
    Home: {
        screen: Home
    },
    Details: {
        screen: DetailScreen
    }
}

const HomeStack = createStackNavigator(screen);

export default createAppContainer(HomeStack)